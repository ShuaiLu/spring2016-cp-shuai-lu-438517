<?php
session_start();
$id=$_POST['id'];
$comment=$_POST['comment'];
if(!isset($_SESSION['username']))
{
    header("Location:read.php?id=$id");
    exit;
}
$username=$_SESSION['username'];
  require 'database.php';
  $collection = $db->comments;
  $cmt = array( 
  	  "id" => $id,
      "username" => $username, 
      "comment" => $comment, 
      "timestamp" => date("h:i:sa")
   );
   $collection->insert($cmt);
   header("Location:read.php?id=$id");  
?>