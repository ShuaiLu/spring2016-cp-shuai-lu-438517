<?php
//post news and insert it to the table.
session_start();
ini_set("session.cookie_httponly", 1);
$headline=$_POST['headline'];
$story=$_POST['story'];
$username=$_SESSION['username'];
require 'database.php';
  $collection = $db->news;
  $document = array( 
      "headline" => $headline, 
      "story" => $story, 
      "username" => $username,
      "timestamp" => date("h:i:sa")
   );
   $collection->insert($document);
    header('Location:index.php');
?>